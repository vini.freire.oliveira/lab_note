class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@labnote.org'
  layout 'mailer'
end
